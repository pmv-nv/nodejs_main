var request = require('request');

request('http://www.km.ru/news/list', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        const $ = require('cheerio').load(body);
        news = '';
        $('.view-content .odd, .view-content .even').each(function(i, $el){
            if (i >= '10'){
                return;
            }
            newsTitle = $('.title', $el).text().trim();
            newsTime = $('.time', $el).text().trim();

            console.log((i+1)+' - '+newsTime+'   '+newsTitle);            

            news += '"'+newsTime+'": "'+newsTitle+'"\n';
        });
        
        require('fs').writeFile('news.json','{\n'+news+'}', function(err) {
            if (err) throw err;
        });   
    };
});